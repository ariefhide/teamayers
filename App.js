import React, {Component} from 'react';
import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator} from 'react-navigation';
import Home from './src/views/home/Home';
import Login from './src/views/login/Login';
import Regist from './src/views/registBaru/Regist';
import ForgotPassword from './src/views/login/ForgotPassword';
import Profile from './src/views/home/Profile';
import Transaction from './src/views/home/Transaction';
import Portofolio from './src/views/home/Portofolio';
import Beli from './src/views/home/pembelian/Beli';
//bella
import ForgotPassByEmail from './src/views/forgotPassword/ForgotPassByEmail';
import ForgotPassByHp from './src/views/forgotPassword/ForgotPassByHp';
import VerifCodeHp from './src/views/forgotPassword/VerifCodeHp';
import NewPassword from './src/views/forgotPassword/NewPassword';

import SideBarHome from './components/header/SideBarHome';
import SideBar from './components/header/SideBar';
import HeaderHome from './components/header/HeaderHome';
import AuthLoading from './components/auth/AuthLoading';
//maytri
import PortofolioJual from './src/views/home/portofolio/PortofoliolJual';
import PortofolioAlihkan from './src/views/home/portofolio/PortofolioAlihkan';

var AppNavigator = createDrawerNavigator({
  Login:Login,
  Regist:Regist,
  ForgotPassword:ForgotPassword,
  Beli:Beli,
  //bella
  ForgotPassByEmail: ForgotPassByEmail,
  ForgotPassByHp:ForgotPassByHp,
  VerifCodeHp: VerifCodeHp,
  NewPassword:NewPassword
},{
  contentComponent: SideBar,
  drawerPosition:'right',
  headerMode: 'float',
  drawerWidth:150
}
)

const DrawerNavigator = createDrawerNavigator({
  Home:Home,
  Profile:Profile,
  Transaction:Transaction,
  Portofolio:Portofolio,
  PortofolioJual: PortofolioJual,
  PortofolioAlihkan: PortofolioAlihkan
},{
  contentComponent: SideBarHome,
  drawerPosition:'right',
  drawerWidth:150,
})

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoading,
    App: AppNavigator,
    Dashboard: DrawerNavigator
  },
  {
    initialRouteName: 'AuthLoading',
  }
));