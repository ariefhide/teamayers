import React, { Component } from 'react';
import { StyleSheet, AppRegistry, Image, ScrollView } from 'react-native';
import { Button, Container, Content, Text, View, Icon, Accordion, Right } from 'native-base';
import { Col, Grid } from 'react-native-easy-grid';
import { Table, Row, Rows } from 'react-native-table-component';
import CardView from 'react-native-cardview';

export default class Profile extends React.Component {
	static navigationOptions = {
		header: null
	};
	constructor(props) {
		super(props);
		this.state = {
			tableHead: [ 'Full Name :', 'inna widia' ],
			tableData: [
				[ 'Nationality :', 'INDONESIA' ],
				[ 'Province :', 'JAWA TENGAH' ],
				[ 'City :', 'ACEH BESAR' ],
				[ '', '' ]
			],
			widthArr: [ 100, 100 ]
		};
	}
	render() {
		const state = this.state;
		return (
			<Container style={{backgroundColor:'#f0eff4'}}>
				<Content>
					{/* Header */}
					<View style={{ flexDirection: 'row', backgroundColor: '#a1887f' }}>
						<Image
							source={require('../../../public/assets/images/ayerswhite.png')}
							style={{ margin: 10, width: '30%', height: 100 }}
							resizeMode={'contain'}
						/>
						<Right>
							<Button transparent>
								<Icon
									onPress={() => this.props.navigation.toggleDrawer()}
									style={{ color: 'white' }}
									name="md-menu"
								/>
							</Button>
						</Right>
					</View>

					<View padder style={{marginVertical: 5}}>
						<ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
							<Grid>
								<Col size={9}>
									<CardView
										cardElevation={4}
										cardMaxElevation={4}
										cornerRadius={3}
										style={{ padding: 5, height: 'auto' }}
									>
										<View style={styles.contentView}>
											<Text style={{ fontSize: 16 }}>Profil Saya</Text>
										</View>
									</CardView>

									<CardView
										cardElevation={4}
										cardMaxElevation={4}
										cornerRadius={3}
										style={{ padding: 5, height: 'auto', marginTop: 5 }}
									>
										<View style={{ paddingLeft: 10, paddingTop: 0, paddingRight: 10 }}>
											<View style={styles.contentView}>
												<Icon name="person" style={styles.icon} />
												<Text style={styles.head}>Informasi Data Diri</Text>
											</View>
											<View style={styles.hr} />

											<Text style={styles.parent}>Nama lengkap</Text>
											<Text style={styles.child}>Andhika</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Kewarganegaraan</Text>
											<Text style={styles.child}>Indonesia</Text>
											<View style={styles.hr} />
											<Text style={styles.parent}>Provinsi</Text>
											<Text style={styles.child}>Jakarta Raya</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Kota</Text>
											<Text style={styles.child}>Jakarta Selatan</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Kecamatan</Text>
											<Text style={styles.child}>Kebagusan</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Alamat</Text>
											<Text style={styles.child}>Jalan Kebagusan</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>No HP</Text>
											<Text style={styles.child}>08123456789</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Pendidikan</Text>
											<Text style={styles.child}>S1</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Foto KTP</Text>
											<Image />
											<View style={styles.hr} />

											<Text style={styles.parent}>Foto NPWP</Text>
											<Image />
											<View style={styles.hr} />

											<View style={styles.contentView}>
												<Icon name="briefcase" style={styles.icon} />
												<Text style={styles.head}>Informasi Data Pekerjaan</Text>
											</View>
											<View style={styles.hr} />

											<Text style={styles.parent}>Pekerjaan</Text>
											<Text style={styles.child}>Karyawan Swasta</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Sumber Dana</Text>
											<Text style={styles.child}>Gaji</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Penghasilan Pertahun</Text>
											<Text style={styles.child}>> 10 - 50 juta / tahun</Text>
											<View style={styles.hr} />

											<View style={styles.contentView}>
												<Icon name="stats" style={styles.icon} />
												<Text style={styles.head}>Informasi Data Bank</Text>
											</View>

											<View style={styles.hr} />

											<Text style={styles.parent}>Nama Bank 1</Text>
											<Text style={styles.child}>BCA</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Nomor Rekening</Text>
											<Text style={styles.child}>123456789</Text>
											<View style={styles.hr} />

											<Text style={styles.parent}>Cabang</Text>
											<Text style={styles.child}>Jakarta Pusat</Text>
											<View style={styles.hr} />
											<Text style={styles.parent}>Rekening Atas Nama</Text>
											<Text style={styles.child}>Andhika</Text>
										</View>
									</CardView>
								</Col>
							</Grid>
						</ScrollView>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	icon: {
		fontSize: 14,
		marginRight: 5,
		marginTop: 7
	},
	head: {
		fontSize: 14,
		marginLeft: 10,
		fontWeight: 'bold',
		marginTop: 5
	},
	hr: {
		borderBottomColor: '#dfdfdf',
		borderBottomWidth: 1
	},
	parent: {
		fontSize: 12,
		marginTop: 10,
		fontWeight: 'bold'
	},
	child: {
		fontSize: 12,
		marginBottom: 10
	},
	contentView: {
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
		marginTop: 5,
		marginBottom: 5
	},
	btn: {
		backgroundColor: '#598fe5',
		justifyContent: 'center',
		width: '94%',
		borderRadius: 5,
		marginTop: 30,
		marginBottom: 20,
		marginLeft: 10
	}
});

AppRegistry.registerComponent('propstate', () => Profile);
