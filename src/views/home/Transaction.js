import React, { Component } from 'react';
import ImagePicker from './ImagePicker';
import { StyleSheet, AppRegistry, TouchableOpacity, ScrollView, Image, Dimensions } from 'react-native';
import { Container, Content, Button, Text, Input, Icon, Card, CardItem, Drawer, Right, View } from 'native-base';
import Modal from 'react-native-modal';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import axios from 'axios';

export default class Transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      collapsedBtnConfirm:false,
      token:null,
      // tableDataApi:[],
      // widthArr:[100,100,100,100,100,100,100],
      // tableHead:['A','B','C','D','E','F','G']
    };
  }
  // componentDidMount() {
  //   console.log(this.props.navigation.state.params.token);
  //   var config={
  //     headers:{
  //       Authorization: 'Bearer ' + this.props.navigation.state.params.token
  //     }
  //   }
  //   console.log(config);
    
  //   axios.get(`http://202.158.32.132:8085/api/user/transaction`, config)
  //     .then((response) => {
  //       console.log(response.data.data);
  //       this.setState({
  //         tableDataApi:response.data.data
  //       })
  //     });
  // }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

  render() {
    // var dataTable = this.state.tableDataApi.map((val,i)=>{
    //   var waktu_order = val.createdAt;
    //   var nama_produk = val.code;
    //   var transaksi = val.type;
    //   var unit = val.unit;
    //   var total_order = val.amount;
    //   var status = val.status;
    //   return(
    //     <Row key={i} data={[`${waktu_order}`,`${nama_produk}`,`${transaksi}`,`${unit}`,`${total_order}`,`${status}`,'']} widthArr={this.state.widthArr}/>
    //   )
    // })
    
    return (
      <Container style={{backgroundColor:'#f0eff4'}}>
        <Content>
          {/* Header */}
          <View style={{ flexDirection: 'row', backgroundColor: '#a1887f' }}>
            <Image source={require('../../../public/assets/images/ayerswhite.png')}
              style={{ margin: 10, width: '30%', height: 100 }} resizeMode={'contain'}
            />
            <Right>
              <Button transparent>
                <Icon onPress={() => this.props.navigation.toggleDrawer()} style={{ color: 'white' }} name='md-menu' />
              </Button>
            </Right>
          </View>
          {/* Content */}
          {/* <ScrollView horizontal={true}>
            <View >
              <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }}>
                <Row data={this.state.tableHead} widthArr={this.state.widthArr} />
                {dataTable}
              </Table>
            </View>
          </ScrollView> */}


          <View padder>
            <Card>
              <CardItem style={{backgroundColor:'lightgrey', flex:1, width:'100%'}}>
                <TouchableOpacity 
                onPress={() =>
                this.setState({
                  collapsedBtnConfirm: !this.state.collapsedBtnConfirm
                })}>
                  <Text style={{ fontSize: 12, fontWeight:'bold', textAlign:'center'}}>
                    Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati (RED)
                  </Text>
                </TouchableOpacity>
              </CardItem>
              
              <Collapse
              isCollapsed={this.state.collapsedBtnConfirm}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnConfirm: isCollapsed })}
              >
              <CollapseHeader></CollapseHeader>
              <CollapseBody>
              <CardItem cardBody>
                <View padder style={{ width: '100%', justifyContent:'center' }}>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Waktu Order</Text>
                    <Text style={styles.textLeft}>: 23/01/2019 17:21 PM</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Transaksi</Text>
                    <Text style={styles.textLeft}>: RED</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Unit</Text>
                    <Text style={styles.textLeft}>: 1000</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Total Nilai Order</Text>
                    <Text style={styles.textLeft}>: 0</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Status</Text>
                    <Text style={[styles.textLeft, {color:'green'}]}>: POSTED</Text>
                  </View>
        
                  <View style={{justifyContent:'center'}}>
                  <Button
                    full
                    style={[styles.btnConfirm, { marginTop: 5 }]}
                    onPress={this._toggleModal}
                  >
                    <Text>Confirm</Text>
                  </Button>
                  </View>
                </View>
                </CardItem>
              </CollapseBody>
              </Collapse>
            </Card>
          </View>

          {/* copas 2 */}
          <View padder style={{marginTop:-20}}>
            <Card>
              <CardItem style={{backgroundColor:'lightgrey', flex:1, width:'100%'}}>
                <TouchableOpacity 
                onPress={() =>
                this.setState({
                  collapsedBtnConfirm: !this.state.collapsedBtnConfirm
                })}>
                  <Text style={{ fontSize: 12, fontWeight:'bold', textAlign:'center'}}>
                    Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati (SUB)
                  </Text>
                </TouchableOpacity>
              </CardItem>
              
              <Collapse
              isCollapsed={this.state.collapsedBtnConfirm}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnConfirm: isCollapsed })}
              >
              <CollapseHeader></CollapseHeader>
              <CollapseBody>
              <CardItem cardBody>
                <View padder style={{ width: '100%', justifyContent:'center' }}>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Waktu Order</Text>
                    <Text style={styles.textLeft}>: 23/01/2019 17:21 PM</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Transaksi</Text>
                    <Text style={styles.textLeft}>: SUB</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Unit</Text>
                    <Text style={styles.textLeft}>: 1000</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Total Nilai Order</Text>
                    <Text style={styles.textLeft}>: 0</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.textLeft, { width: 110 }]}>Status</Text>
                    <Text style={[styles.textLeft,{color:'green'}]}>: POSTED</Text>
                  </View>
        
                  <View style={{justifyContent:'center'}}>
                  <Button
                    full
                    style={[styles.btnConfirm, { marginTop: 5 }]}
                    onPress={this._toggleModal}
                  >
                    <Text>Confirm</Text>
                  </Button>
                  </View>
                </View>
                </CardItem>
              </CollapseBody>
              </Collapse>
            </Card>
          </View>

          <Modal
            isVisible={this.state.isModalVisible}
            onBackButtonPress={() => this.setState({ isModalVisible: false })}
            style={{ justifyContent: 'flex-start' }}
          >
            <Content>
              <View style={styles.modal}>
                <Text style={styles.textLeft}>
                  Product : Reksa Dana Syariah PAM Syariah Campuran Dana Daqu
			      			</Text>
                <Text style={[styles.textLeft, { fontWeight: 'bold', marginVertical: 5 }]}>
                  Nominal Pembelian : 50.000
			      			</Text>
                <Text style={styles.textLeft}>Konfirmasi Pembayaran</Text>
                <ImagePicker />
                <Button onPress={this._toggleModal} style={styles.btnSubmit}>
                  <Text style={{ fontSize: 12 }}>Submit</Text>
                </Button>
              </View>
            </Content>
          </Modal>

        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  head: {
    height: 40,
    backgroundColor: '#A1887F'
  },
  textHead: {
    textAlign: 'center',
    fontSize: 12,
    fontWeight: 'bold'
  },
  text: {
    margin: 6,
    textAlign: 'center',
    fontSize: 12
  },
  textLeft: {
    margin: 2,
    fontSize: 12
  },
  row: {
    flexDirection: 'row'
  },
  btn: {
    width: 76,
    height: 25,
    backgroundColor: '#007bff',
    borderRadius: 3,
    justifyContent: 'center'
  },
  btnConfirm: {
    height: 35,
    backgroundColor: '#007bff',
    borderRadius: 3,
    justifyContent: 'center'
  },
  btnText: {
    fontSize: 10,
    textAlign: 'center',
    color: '#fff'
  },
  modal: {
    backgroundColor: '#ffffff',
    borderRadius: 3,
    padding: 10
  },
  btnSubmit: {
    backgroundColor: '#A1887F',
    color: '#fff',
    borderRadius: 3,
    height: 35,
    justifyContent: 'center'
  }
});

AppRegistry.registerComponent('propstate', () => Transaction);