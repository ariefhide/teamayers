import React, {Component} from 'react';
import {StyleSheet, AppRegistry, Text, View, Image} from 'react-native';
import MultiStep from 'react-native-multistep-wizard'
import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import {Row, Col, Grid} from 'react-native-easy-grid';

export default class Beli extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            steps: [
                {
                    name: 'Step1',
                    component: <Step1/>
                }, {
                    name: 'Step2',
                    component: <Step2/>
                }, {
                    name: 'Step3',
                    component: <Step3/>
                }
            ]
        }
    }

    finish = (wizardState) => {
        this.props.navigation.navigate('Transaction')
    }

    render() {
        const {steps} = this.state
        return (
            <View>
                <Grid>
                    <Col size={1} ></Col>
                    <Col size={8}>
                        <View style={styles.border} >
                            <MultiStep steps={steps} onFinish={this.finish}/>
                        </View>
                    </Col>
                    <Col size={1} ></Col>
                </Grid>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    border:{
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 7,
        backgroundColor:'white',
        borderColor:'white',
        borderWidth:1,
        borderRadius:3,
        padding:10,
        height:550,
        marginTop:20
    },
});

AppRegistry.registerComponent('propstate', () => Beli);
