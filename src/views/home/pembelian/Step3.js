import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View} from 'react-native';
import {Button} from 'native-base';
import {Row, Col, Grid} from 'react-native-easy-grid';
import {CheckBox} from 'react-native-elements';
import Modal from "react-native-modal";

  export default class Step3 extends Component{

    constructor(props){
      super(props);
      this.state = { checked:false,isModalVisible: false,}
  }
  _toggleModal = () => {
      this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  handleChecked = () =>{
    this.setState({checked:!this.state.checked})
}
  

    render() {
      const { checked } = this.state
        return (
          <View>
            <Modal
              isVisible={this.state.isModalVisible}
              animationInTiming={500}
              animationOutTiming={500}
              onBackdropPress={() => this.setState({ isModalVisible: false })}
              style={{
                backgroundColor:'white',
                padding:20,
                marginTop:300,
                borderRadius:5,
              }}
            >
              <Text style={{fontSize:16,marginBottom:5}} >Proses Pembelian Anda Berhasil</Text>
              <Text style={{fontSize: 12}}>Nilai NAB/ Unit yang Anda dapat akan kami proses maksimal 1 x 24 jam sesuai dengan syarat dan ketentuan yang berlaku.</Text>
              <Button style={styles.btn3} onPress={this._toggleModal}>
                <Text style={{color:'white', fontSize: 12}}>OK</Text>
              </Button>
          </Modal>
            <Grid style={{marginBottom:30}} >
              <Col><Text style={styles.border} >Pilih Product</Text></Col>
              <Col><Text style={styles.border}>Resiko Profile</Text></Col>
              <Col><Text style={styles.borderCoklat}>Proses Pembelian</Text></Col>
            </Grid>

            <View style={styles.border} >
              <Text style={{textAlign:'center',fontSize:14 ,fontWeight: "bold"}} >Selalu waspada terhadap pihak tidak bertanggung jawab</Text>
              <Text style={{textAlign:'center',fontSize:12}} >Lakukan pembayaran pembelian reksadana Anda sesuai dengan nominal pembelian. </Text>
              <Text style={{textAlign:'center',fontSize:12,marginBottom:30}} >Pastikan rekening yang Anda gunakan untuk melakukan pembayaran adalah rekening Anda saat proses registrasi dan dikirim ke :</Text>
              <Text>Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati</Text>
              <View style={styles.contentView} >
                <Text style={{fontSize: 12}}>No. Rekening : </Text>
                <Text style={{fontWeight:'bold', fontSize: 12}} >2259801347</Text>
              </View>
              <View style={styles.contentView} >
                <Text style={{fontSize: 12}}>Nama Rekening : </Text>
                <Text style={{fontWeight:'bold', fontSize: 12}} >RD Ayers Equity Index Sri Kehati</Text>
              </View>
              <View style={styles.contentView} >
                <Text style={{fontSize: 12}}>Bank : </Text>
                <Text style={{fontSize: 12}}>PT Bank Maybank Indonesia Cab. Sentral Senayan II</Text>
              </View>
              <View style={styles.contentView} >
                <Text style={{fontWeight:'bold', fontSize: 12}} >Total : </Text>
                <Text style={{fontWeight:'bold', fontSize: 12}} >IDR 100000</Text>
              </View>

              <View style={styles.contentView2} >
                <CheckBox containerStyle={{borderColor:'white',backgroundColor:'white'}} checkedColor='#167bff' checked={checked} title='Setuju' onPress={this.handleChecked} />
              </View>
              <View style={styles.contentView2} >
                {
                  checked ?
                  <Button style={styles.btn} onPress={this._toggleModal} >
                      <Text style={{color:'white', fontSize: 12}} >Proses Pembelian</Text>
                  </Button>
                  :
                  <Button style={styles.btn2} disabled >
                      <Text style={{color:'white', fontSize: 12}} >Proses Pembelian</Text>
                  </Button>
                }
              </View>   
            </View>

          </View>
        );
      }
    }

const styles = StyleSheet.create({
  contentView: {
    flex: 1,
    flexDirection:'row',
    flexWrap:'wrap',
    marginTop:5
  },
  contentView2: {
    flex: 1,
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'center',
    marginTop:-5
  },
  border:{
      borderWidth:1,
      borderRadius:3,
      borderColor:'#e5e5e5',
      padding:10,
      textAlign:'center',
      fontSize: 16
  },
  borderCoklat:{
      borderWidth:1,
      borderRadius:3,
      borderColor:'#e5e5e5',
      padding:10,
      textAlign:'center',
      backgroundColor:'#a1887f',
      color:'white',
      fontSize: 16
  },
  btn:{
      backgroundColor:'#167bff',
      width:'50%',
      borderRadius:3,
      justifyContent:'center'
  },
  btn2:{
      backgroundColor:'#5faaff',
      width:'50%',
      borderRadius:3,
      justifyContent:'center'
  },
  btn3:{
    backgroundColor:'#167bff',
    width:'100%',
    borderRadius:3,
    justifyContent:'center',
    marginTop:40,
    
},
});
