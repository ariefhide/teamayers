import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image,TextInput} from 'react-native';
import {Button} from 'native-base';
import {Row, Col, Grid} from 'react-native-easy-grid';

  export default class Step1 extends Component{

  nextPreprocess=()=>{
    this.props.saveState(1,{key:'value'})
    this.props.nextFn()
  }
  
    render() {
        return (
        <View>
            <Grid style={{marginBottom:30}} >
                <Col><Text style={styles.borderCoklat} >Pilih Product</Text></Col>
                <Col><Text style={styles.border}>Resiko Profile</Text></Col>
                <Col><Text style={styles.border}>Proses Pembelian</Text></Col>
            </Grid>
            <View style={styles.border2} >
                <Text style={{fontSize: 12}}>Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati</Text>
                <Text style={{fontSize: 12}}>NAB/Unit per Tanggal 2018-11-13T15:34:13. IDR 1000</Text>
            </View>
            <View style={styles.border2} >
                <Text style={{fontSize: 12}}>Nominal Pembelian(IDR)</Text>
                <TextInput 
                    placeholder="100000" 
                    style={[styles.input, {fontSize: 12}]}
                    keyboardType="numeric" 
                />
                <Button onPress={this.nextPreprocess} style={styles.btn} >
                    <Text style={{color:'white', fontSize: 12}} >Masukan</Text>
                </Button>
            </View>
        </View>  
        );
      }
    }

    const styles = StyleSheet.create({
        border:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
            textAlign:'center',
            fontSize: 16
        },
        borderCoklat:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
            textAlign:'center',
            backgroundColor:'#a1887f',
            color:'white',
            fontSize: 16
        },
        border2:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
        },
        btn:{
            backgroundColor:'#167bff',
            width:'100%',
            borderRadius:3,
            justifyContent:'center',
        },
        input:{
            height: 40, 
            borderColor: '#e5e5e5', 
            borderWidth: 1,
            borderRadius:3,
            marginTop:10,
            marginBottom:10
        }
    });