import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';
import {Button} from 'native-base';
import {Row, Col, Grid} from 'react-native-easy-grid';
import PieChart from 'react-native-pie-chart';
import {CheckBox} from 'react-native-elements';

  export default class Step2 extends Component{
      constructor(props){
          super(props);
          this.state = { checked:false}
      }

  nextPreprocess=()=>{
    this.props.saveState(2,{key:'value'})
    this.props.nextFn()
  }
  previousPreprocess=()=>{
            
    this.props.prevFn()
        
  }
  handleChecked = () =>{
      this.setState({checked:!this.state.checked})
  }
  
    render() {
        const chart_wh = 250
        const series = [20,80]
        const sliceColor = ['#c56363','#a00000']
        const { checked } = this.state;
        return (
            <View>
                <Grid style={{marginBottom:30}} >
                    <Col><Text style={styles.border} >Pilih Product</Text></Col>
                    <Col><Text style={styles.borderCoklat}>Resiko Profile</Text></Col>
                    <Col><Text style={styles.border}>Proses Pembelian</Text></Col>
                </Grid>
                <View style={styles.border2} >     
                    <Text style={{fontSize: 12}}>Di bawah ini merupakan profil resiko investasi yang menggambarkan produk - produk investasi yang sesuai dengan Anda berdasarkan nilai pada kuesioner yang telah Anda jawab pada saat melakukan registrasi</Text>
                </View>
                <View style={styles.border3} >
                    <Text style={{fontWeight: "bold",fontSize:14,marginTop:10}} >Profil Investasi</Text>
                    <Text style={styles.hr} >
                        29-32 Pengambil Resiko
                    </Text>
                    <Text style={{fontWeight: "bold",fontSize:14}} >Jenis Produk</Text>
                    <Text style={styles.hr} >Reksa Dana Saham</Text>
                    <Text style={{fontWeight: "bold",fontSize:14}} >Produk Investasi</Text>

                    <View style={styles.contentView}>
                        <View style={{width:30,height:12,marginTop:3,marginRight:3,backgroundColor:'#c56363'}} ></View>
                        <Text style={{fontSize: 12}}>Pasar Uang</Text>
                        <View style={{width:30,height:12,marginTop:3,marginRight:3,marginLeft:6,backgroundColor:'#a00000'}} ></View>
                        <Text style={{fontSize: 12}}>Saham</Text>
                    </View>
                    
                    <PieChart
                        chart_wh={chart_wh}
                        series={series}
                        sliceColor={sliceColor}
                        doughnut={true}
                        coverRadius={0.45}
                        coverFill={'#FFF'}
                        style={{width:'50%',marginTop:10,marginBottom:10}}
                    />
                    <Text style={{fontWeight: "bold",fontSize:14,marginTop:10,marginBottom:10}} >Tipe Investor</Text>
                    <Text style={{fontSize: 12}}>Kerugian tinggi terhadap resiko kerugian yang signifikan dan fluktuasi negatif untuk memperoleh pertumbuhan dan pendapatan yang tinggi dalam jangka waktu 5 tahun atau lebih</Text>
                </View>
                <View style={styles.contentView} >
                    <CheckBox containerStyle={{borderColor:'white',backgroundColor:'white'}} checkedColor='#167bff' checked={checked} title='Setuju' onPress={this.handleChecked} />
                </View>
                <View style={styles.contentView} >
                    {
                        checked ?
                        <Button style={styles.btn} onPress={this.nextPreprocess}>
                            <Text style={{color:'white', fontSize: 12}} >Proses Transaksi</Text>
                        </Button>
                        :
                        <Button style={styles.btn2} disabled >
                            <Text style={{color:'white', fontSize: 12}} >Proses Transaksi</Text>
                        </Button>
                    }
                </View>    
            </View>
        );
      }
    }
const styles = StyleSheet.create({
        contentView: {
            flex: 1,
            flexDirection:'row',
            flexWrap:'wrap',
            justifyContent:'center',
            marginTop:10
        },
        hr:{
            borderBottomColor: '#e5e5e5',
            borderBottomWidth: 1,
            marginBottom:10
        },
        border:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
            textAlign:'center',
            fontSize: 16
        },
        borderCoklat:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
            textAlign:'center',
            backgroundColor:'#a1887f',
            color:'white',
            fontSize: 16
        },
        border2:{
            borderWidth:0,
            borderRadius:3,
            padding:10,
            backgroundColor:'#ffeeba',
            color:'#856404',
            marginTop:-10
        },
        border3:{
            borderWidth:1,
            borderRadius:3,
            borderColor:'#e5e5e5',
            padding:10,
            marginTop:20
        },
        btn:{
            backgroundColor:'#167bff',
            width:'50%',
            borderRadius:3,
            justifyContent:'center'
        },
        btn2:{
            backgroundColor:'#5faaff',
            width:'50%',
            borderRadius:3,
            justifyContent:'center'
        }
    });
