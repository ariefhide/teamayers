import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { Container, View, Content, Button, Text, Input, Icon, Item, Label } from 'native-base';
import ImagePicker from 'react-native-image-picker';

export default class Test1 extends React.Component {
	state = {
		avatarSource: null
	};
	selectImage = async () => {
		ImagePicker.showImagePicker({ noData: true, mediaType: 'photo' }, (response) => {
			console.log('Response = ', response);

			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			} else {
				this.setState({
					avatarSource: response.uri
				});
			}
		});
	};

	render() {
		return (
			<View>
				{this.state.avatarSource && (
					<Image
						source={{ uri: this.state.avatarSource }}
						style={{ width: '80%', height: 200, resizeMode: 'contain', marginHorizontal: 30 }}
					/>
				)}
				<View disabled style={{ flexDirection: 'row', borderWidth: 1, borderRadius: 3, marginVertical: 5 }}>
					<Input disabled type="file" placeholder={this.state.avatarSource} style={{fontSize: 12, alignItems: 'center', height: 35, padding: 0, borderColor:'#b7b7b7' }}/>
					<Button onPress={this.selectImage} style={styles.btnSelect}>
						<Text style={{ fontSize: 12 }}>Browse</Text>
					</Button>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	btnSelect: {
		backgroundColor: '#b7b7b7',
		color: '#fff',
		borderRadius: 3,
		height: 35
	}
});
