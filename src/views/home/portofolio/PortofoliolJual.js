import React, { Component } from 'react';
import { StyleSheet, AppRegistry, TouchableOpacity, Image } from 'react-native';
import {
	Container,
	Content,
	Card,
	CardItem,
	Text,
	View,
	Button,
	Input,
	Item,
	CheckBox,
	Left,
	Right,
	Icon
} from 'native-base';
import Modal from 'react-native-modal';
import LineChart from 'react-native-responsive-linechart';

export default class PortofolioJual extends Component {
	state = {
		isModalVisible: false
	};

	_toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

	render() {
		const data = [-10, -15, 40, 19, 32, 15, 52, 55, 20, 60, 78, 42, 56];
		const config = {
			line: {
				strokeWidth: 1,
				strokeColor: '#216D99'
			},
			area: {
				gradientFrom: '#2e86de',
				gradientFromOpacity: 1,
				gradientTo: '#87D3FF',
				gradientToOpacity: 1
			},
			yAxis: {
				labelColor: '#c8d6e5'
			},
			grid: {
				strokeColor: '#c8d6e5',
				stepSize: 30
			},
			insetY: 10,
			insetX: 10,
			interpolation: 'spline',
			backgroundColor: '#fff'
		};

		return (
			<Container style={{backgroundColor:'#f0eff4'}}>
				<Content>
					<View style={{ flexDirection: 'row', backgroundColor: '#a1887f' }}>
					  <Image source={require('../../../../public/assets/images/ayerswhite.png')}
					    style={{ margin: 10, width: '30%', height: 100 }} resizeMode={'contain'}
					  />
					  <Right>
					    <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
					      <Icon style={{ color: 'white' }} name='md-menu' />
					    </Button>
					  </Right>
					</View>

					<View padder>
						<Card transparent>
							<CardItem>
								<View style={{ width: '100%' }}>
									<View>
										<Text style={{ fontSize: 14 }}>
											Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati
										</Text>
										<Text style={{ fontSize: 12 }}>RD Ayers Equity Index Sri Kehati</Text>
									</View>
									<View style={{ marginVertical: 10 }}>
										<Text style={styles.textCard}>NAB/Unit (2018-11-13 15:34:13)</Text>
										<Text style={styles.textCard}>IDR 1.000, -10.63118756032%</Text>
									</View>
								</View>
							</CardItem>
						</Card>
						<Card transparent>
							<LineChart style={{ height: 200 }} config={config} data={data} />
						</Card>
						<Card transparent>
							<CardItem>
								<View style={{ width: '100%' }}>
									<View>
										<Text style={{ fontSize: 14 }}>Jual Reksa Dana</Text>
										<Text style={styles.textCard}>Hasil Penjualan akan ditransfer ke</Text>
										<Text style={{ fontSize: 12 }}>Nama Bank : ANZ</Text>
										<Text style={{ fontSize: 12 }}>Cabang : Cilandak</Text>
										<Text style={{ fontSize: 12 }}>Nomor Rekening : 45545455334</Text>
									</View>
									<View style={{ marginVertical: 10 }}>
										<Text style={styles.textCard}>Estimasi Nilai Reksadana : IDR 20.000</Text>
										<Text style={{ fontSize: 12 }}>Token</Text>
										<Item regular style={styles.input}>
											<Input style={{ fontSize: 13 }} />
										</Item>
									</View>
									<View style={{ flexDirection: 'row' }}>
										<Left>
											<Button
												full
												transparent
												onPress={() => {
													this.props.navigation.navigate('Portofolio');
												}}
												style={{
													backgroundColor: '#A1887F',
													height: 35,
													borderRadius: 3,
													marginRight: 5
												}}
											>
												<Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>
													Batal
												</Text>
											</Button>
										</Left>
										<Right>
											<Button
												full
												transparent
												onPress={this._toggleModal}
												style={{ backgroundColor: '#A1887F', height: 35, borderRadius: 3 }}
											>
												<Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>
													Konfirmasi
												</Text>
											</Button>
										</Right>
									</View>
									<View style={{ flex: 1 }}>
										<Modal
											isVisible={this.state.isModalVisible}
											style={{ justifyContent: 'flex-start' }}
										>
											<View style={styles.modal}>
												<Text style={{ fontSize: 14, alignItems: 'center', justifyContent: 'center' }}>
													Penjualan Anda akan kami proses
												</Text>
												<Button onPress={this._toggleModal} style={styles.btnOK}>
													<Text style={{ fontSize: 12 }}>OK</Text>
												</Button>
											</View>
										</Modal>
									</View>
								</View>
							</CardItem>
						</Card>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		fontSize: 12
	},
	textCard: {
		fontSize: 12,
		fontWeight: 'bold'
	},
	input: {
		height: 35,
		borderRadius: 3,
		marginVertical: 5,
		borderWidth: 1
	},
	modal: {
		backgroundColor: '#ffffff',
		borderRadius: 3,
		padding: 10
	},
	btnOK: {
		backgroundColor: '#A1887F',
		color: '#fff',
		borderRadius: 3,
		height: 35,
		width: 53,
		marginTop: 10,
		justifyContent: 'center'
	}
});
