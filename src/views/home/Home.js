import React, { Component } from 'react';
import { StyleSheet, AppRegistry, Image, ScrollView } from 'react-native';
import { Button, Container, Content, Text, View, Drawer, Right, Icon } from 'native-base';
import { Row, Col, Grid } from 'react-native-easy-grid';
import Axios from 'axios';

export default class Home extends React.Component {

  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      produks: [],
      idCoret: '',
      coret: false,
      token:null
    }
  }

  componentDidMount() {
    console.log(this.props.navigation.getParam('Dashboard', 'token'));
    
    Axios.get(`http://202.158.32.134:17051/Radsoft/RDO/GetMasterFund/RDOAPI/RDAYR3`)
      .then(res => {
        console.log(res.data.data, 'ini res produk');
        this.setState({ produks: res.data.data });
      });
  }
  coret = (e, produk) => {
    if (this.state.coret === false) {
      this.setState({ idCoret: produk, coret: !this.state.coret })
    } else {
      this.setState({ idCoret: '', coret: !this.state.coret })
    }
  }
  render() {
    const { produks, idCoret } = this.state
    return (
      <ScrollView
        style={styles.contentContainer}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ flexDirection: 'row', backgroundColor: '#a1887f' }}>
          <Image source={require('../../../public/assets/images/ayerswhite.png')}
            style={{ margin: 10, width: '30%', height: 100 }} resizeMode={'contain'}
          />
          <Right>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
              <Icon style={{ color: 'white' }} name='md-menu' />
            </Button>
          </Right>
        </View>

        <Grid>
          <Col size={0.25} ></Col>
          <Col size={9.5} >
            <View style={styles.shadow3} >
              <Text style={styles.fontLarge}>RP. 0</Text>
              <Text style={styles.fontLarge}>Total Investasi</Text>
            </View>
            <View style={styles.shadow3} >
              <Text style={styles.fontLarge}>RP. 0</Text>
              <Text style={styles.fontLarge}>Transaksi Pending</Text>
            </View>

            <View style={styles.shadow2} >
              <Text style={{ marginBottom: 10, fontSize: 16, marginLeft: 10, fontWeight:'bold' }} >Produk Kami</Text>

              {
                produks.map(produk => (
                  <View style={styles.shadow} key={produk.Code} >
                    <View style={{ marginLeft: 10, marginTop: 10 }} >
                      <Text style={styles.fontMedium}>{produk.FundName}</Text>
                      <Text style={styles.fontSmall}>{produk.RekName}</Text>
                      <Text style={styles.fontSmall}>NAB/Unit per Tanggal {produk.LastUpdate} IDR {produk.LastNAV}</Text>
                    </View>
                    <Button style={styles.btn} onPress={() => this.props.navigation.navigate('Beli')} ><Text>Beli</Text></Button>
                    <View >
                      <Text onPress={(e) => { this.coret(e, produk.Code) }} style={{ fontSize: 12, textAlign: 'center', textDecorationLine: `${idCoret === produk.Code ? 'line-through' : 'none'}` }}>{produk.FundName}</Text>
                      {
                        idCoret === produk.Code ?
                          null
                          :
                          <Image source={require('../../../public/assets/images/charss.png')} style={{ width: '93%', marginTop: 10, marginLeft: 10 }} />
                      }
                    </View>
                  </View>
                ))}
            </View>
          </Col>
          <Col size={0.25} ></Col>
        </Grid>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#167bff',
    justifyContent: 'center',
    width: '94%',
    height: 35,
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 7,
    backgroundColor: 'white',
    marginBottom: 10,
    opacity: 1,
    borderRadius: 3,
    paddingTop: 5,
    paddingBottom: 5
  },
  shadow2: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.5,
    shadowRadius: 6.68,

    elevation: 11,
    backgroundColor: 'white',
    width: '100%',
    padding: 10,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 3
  },
  shadow3: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 7,
    backgroundColor: 'white',
    width: '100%',
    padding: 10,
    marginTop: 3,
    borderRadius: 3
  },
  fontLarge: {
    fontSize : 16
  },
  fontMedium: {
    fontSize : 14
  },
  fontSmall: {
    fontSize : 12
  }
})

AppRegistry.registerComponent('propstate', () => Home);
