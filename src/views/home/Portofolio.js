import React, { Component } from 'react';
import { StyleSheet, AppRegistry, Picker, Image, Dimensions } from 'react-native';
import { Container, Content, Card, CardItem, Text, View, Button, Input, Item, CheckBox, Drawer, Right, Icon, Left } from 'native-base';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Dropdown } from 'react-native-material-dropdown';
import {Col, Row, Grid} from 'react-native-easy-grid';

export default class Portofolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsedBtnJual: false,
      collapsedBtnAlihkan: false,
      checkedJual: false,
      checkedAlihkan: false,
      alihkan: false,
      item: null,
    };
  }

  static navigationOptions = {
    header: null
  }

  updateItem = (item) => {
    this.setState({ item: item, alihkan: true });
  };

  render() {
    const { tampil, alihkan } = this.state;
    let data = [
      {
        value: 'Reksadana Ayers Asia Asset Management Government Bond Fund'
      },
      {
        value: 'Reksa Dana Ayers Asia Asset Management Money Market Fund'
      }
    ];
    return (
      <Container style={{backgroundColor:'#f0eff4'}}>
        <Content>
          <View style={{ flexDirection: 'row', backgroundColor: '#a1887f' }}>
            <Image source={require('../../../public/assets/images/ayerswhite.png')}
              style={{ margin: 10, width: '30%', height: 100 }} resizeMode={'contain'}
            />
            <Right>
              <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
                <Icon style={{ color: 'white' }} name='md-menu' />
              </Button>
            </Right>
          </View>
          <View padder>
            <Card transparent>
              <CardItem style={{backgroundColor:'lightgrey', flex:1, width:'100%'}}>
                  <Text style={{ fontSize: 13, fontWeight:'bold', textAlign:'center'}}>
                    Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati
                  </Text>
              </CardItem>
              <CardItem cardBody>
                <View style={{ width: '100%' }}>
                  <View style={{ marginVertical: 10, marginHorizontal:15 }}>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>NAV Date</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 2019-09-18</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>Total Investasi</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: Rp. 2.000.000</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>Unit</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 1000</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>NAV Value</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 1200</Text></Col>
                    </Grid>
                  </View>
                  
                  <View>
                    <Grid>
                      <Col size={1}></Col>
                      <Col size={4}>
                        <View style={{ marginTop: 0.5 }}>
                        <Left>
                          <Button 
                            transparent
                            onPress={() =>
                              this.setState({
                                collapsedBtnJual: !this.state.collapsedBtnJual,
                                collapsedBtnAlihkan: false,
                                tampil: true
                              })}
                            style={{
                              backgroundColor: '#007BFF',
                              height: 35,
                              width:100,
                              borderRadius: 3,
                              marginTop: 5,
                              marginBottom:15,
                              justifyContent: 'center'
                            }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Jual</Text>
                          </Button>
                          </Left>
                        </View>
                      </Col>
                      <Col size={4}>
                        <View style={{ marginTop: 0.5 }}>
                        <Right>
                          <Button 
                            transparent
                            onPress={() =>
                              this.setState({
                                collapsedBtnAlihkan: !this.state.collapsedBtnAlihkan,
                                collapsedBtnJual: false,
                                tampil: true
                              })}
                            style={{
                              justifyContent: 'center',
                              height: 35,
                              width:100,
                              borderRadius: 3,
                              marginTop: 5,
                              marginBottom:15,
                              backgroundColor:'#df514f'
                            }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Alihkan</Text>
                          </Button>
                          </Right>
                      </View>
                      </Col>
                      <Col size={1}></Col>
                    </Grid>
                  </View>
                </View>
              </CardItem>
            </Card>
            <Collapse
              isCollapsed={this.state.collapsedBtnJual}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnJual: isCollapsed })}
            >
              <CollapseHeader />
              <CollapseBody style={{marginTop:-9}}>
                <Card transparent>
                <CardItem style={{ flexDirection: 'column' }}>
                    <View style={{ width: '100%' }}>
                      <View>
                        <Text style={{ fontSize: 12 }}>Jumlah Unit yang Akan Dijual</Text>
                        <Item regular style={styles.input}>
                          <Input style={{ fontSize: 12 }} />
                        </Item>
                        <View style={{ flexDirection: 'row', marginLeft: -7 }}>
                          <CheckBox
                            style={{ borderRadius: 3, borderWidth: 1 }}
                            checked={this.state.checkedJual}
                            onPress={() =>
                              this.setState({ checkedJual: !this.state.checkedJual })}
                          />
                          <Text style={{ fontSize: 12, marginLeft: 15, marginTop: 2 }}>
                            Semua Unit
                          </Text>
                        </View>
                      </View>

                      <View style={{ marginVertical: 10 }}>
                        <Text style={{ fontSize: 12 }}>Estimasi Nilai Reksadana</Text>
                        <Text style={{ fontSize: 12 }}>IDR. 0</Text>
                      </View>
                      <View style={{ marginBottom: 10 }}>
                        <Text style={{ fontSize: 12 }}>Minimum Penjualan</Text>
                        <Text style={{ fontSize: 12 }}>IDR. 0</Text>
                      </View>
                      
                      <Button
                        transparent
                        full
                        onPress={() => {
                          this.props.navigation.navigate('PortofolioJual');
                        }}
                        style={{ backgroundColor: '#007BFF', borderRadius: 3, height:35 }}
                      >
                        <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Jual</Text>
                      </Button>
                    </View>
                  </CardItem>
                </Card>
              </CollapseBody>
            </Collapse>
            <Collapse
              isCollapsed={this.state.collapsedBtnAlihkan}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnAlihkan: isCollapsed })}
            >
              <CollapseHeader />
              <CollapseBody style={{marginTop:-9}}>
                <Card transparent>
                  <CardItem style={{ flexDirection: 'column' }}>
                    <View style={{ width: '100%' }}>
                      <View>
                        <Text style={styles.text}>Jumlah Unit yang Akan Dialihkan</Text>
                        <Item regular style={styles.input}>
                          <Input />
                        </Item>
                        <View style={{ flexDirection: 'row', marginLeft: -7 }}>
                          <CheckBox
                            style={{ borderRadius: 3 }}
                            checked={this.state.checkedAlihkan}
                            onPress={() =>
                              this.setState({ checkedAlihkan: !this.state.checkedAlihkan })}
                          />
                          <Text style={{ fontSize: 12, marginLeft: 15, marginTop: 2 }}>
                            Semua Unit
                            </Text>
                        </View>
                      </View>
                      
                      <View>
                        <Dropdown
                          label="Dialihkan ke"
                          data={data}
                          onChangeText={this.updateItem}
                          style={{ fontSize: 12 }}
                        />
                      </View>

                      {alihkan ? (
                        <View>
                          <View style={{ marginVertical: 10 }}>
                            <Text style={styles.text}>NAB/Unit (2018-11-13 15:34:17)</Text>
                            <Text style={styles.text}>IDR. 1024.362887</Text>
                          </View>
                          <View>
                            <Text style={styles.text}>Biaya Pengalihan</Text>
                            <Text style={styles.text}>0.02%</Text>
                          </View>
                          <View style={{ marginVertical: 10 }}>
                            <Text style={styles.text}>Minimum Pengalihan</Text>
                            <Text style={styles.text}>IDR. 20.000</Text>
                          </View>                          
                          <Button
                            full
                            transparent
                            onPress={() => {
                              this.props.navigation.navigate('PortofolioAlihkan');
                            }}
                            style={{ height: 35, borderRadius: 3, backgroundColor:'#df514f' }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>
                              Alihkan
                              </Text>
                          </Button>
                        </View>
                      ) : null}
                    </View>
                  </CardItem>
                </Card>
              </CollapseBody>
            </Collapse>
          </View>

          {/* part 2 */}
          <View padder style={{marginTop:-15}}>
            <Card transparent>
              <CardItem style={{backgroundColor:'lightgrey', flex:1, width:'100%'}}>
                  <Text style={{ fontSize: 13, fontWeight:'bold', textAlign:'center'}}>
                    Reksa Dana Indeks Ayers Asia Asset Management Equity Index Sri Kehati
                  </Text>
              </CardItem>
              <CardItem cardBody>
                <View style={{ width: '100%' }}>
                  <View style={{ marginVertical: 10, marginHorizontal:15 }}>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>NAV Date</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 2019-09-18</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>Total Investasi</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: Rp. 2.000.000</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>Unit</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 1000</Text></Col>
                    </Grid>
                    <Grid>
                      <Col size={4}><Text style={{fontSize:12}}>NAV Value</Text></Col>
                      <Col size={6}><Text style={styles.textCard}>: 1200</Text></Col>
                    </Grid>
                  </View>
                  
                  <View>
                    <Grid>
                      <Col size={1}></Col>
                      <Col size={4}>
                        <View style={{ marginTop: 0.5 }}>
                        <Left>
                          <Button
                            transparent
                            onPress={() =>
                              this.setState({
                                collapsedBtnJual: !this.state.collapsedBtnJual,
                                collapsedBtnAlihkan: false,
                                tampil: true
                              })}
                            style={{
                              backgroundColor: '#007BFF',
                              height: 35,
                              width:100,
                              borderRadius: 3,
                              marginTop: 5,
                              marginBottom:5,
                              justifyContent: 'center',
                            }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Jual</Text>
                          </Button>
                          </Left>
                        </View>
                      </Col>
                      <Col size={4}>
                        <View style={{ marginTop: 0.5 }}>
                        <Right>
                          <Button
                            transparent
                            onPress={() =>
                              this.setState({
                                collapsedBtnAlihkan: !this.state.collapsedBtnAlihkan,
                                collapsedBtnJual: false,
                                tampil: true
                              })}
                            style={{
                              justifyContent: 'center',
                              height: 35,
                              width:100,
                              borderRadius: 3,
                              marginTop: 5,
                              marginBottom:5,
                              backgroundColor:'#df514f'
                            }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Alihkan</Text>
                          </Button>
                          </Right>
                      </View>
                      </Col>
                      <Col size={1}></Col>
                    </Grid>
                  </View>
                </View>
              </CardItem>
            </Card>
            <Collapse
              isCollapsed={this.state.collapsedBtnJual}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnJual: isCollapsed })}
            >
              <CollapseHeader />
              <CollapseBody style={{marginTop:-9}}>
                <Card transparent>
                  <CardItem style={{ flexDirection: 'column' }}>
                    <View style={{ width: '100%' }}>
                      <View>
                        <Text style={{ fontSize: 13 }}>Jumlah Unit yang Akan Dijual</Text>
                        <Item regular style={styles.input}>
                          <Input style={{ fontSize: 13 }} />
                        </Item>
                        <View style={{ flexDirection: 'row', marginLeft: -7 }}>
                          <CheckBox
                            style={{ borderRadius: 3, borderWidth: 0 }}
                            checked={this.state.checkedJual}
                            onPress={() =>
                              this.setState({ checkedJual: !this.state.checkedJual })}
                          />
                          <Text style={{ fontSize: 12, marginLeft: 15, marginTop: 2 }}>
                            Semua Unit
                            </Text>
                        </View>
                        <View style={{ marginVertical: 10 }}>
                          <Text style={{ fontSize: 12 }}>Estimasi Nilai Reksadana</Text>
                          <Text style={{ fontSize: 12 }}>IDR. 0</Text>
                        </View>
                        <View style={{ marginBottom: 10 }}>
                          <Text style={{ fontSize: 12 }}>Minimum Penjualan</Text>
                          <Text style={{ fontSize: 12 }}>IDR. 0</Text>
                        </View>
                      </View>
                      <Button
                        transparent
                        full
                        onPress={() => {
                          this.props.navigation.navigate('PortofolioJual');
                        }}
                        style={{ backgroundColor: '#007BFF', borderRadius: 3, height:35 }}
                      >
                        <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>Jual</Text>
                      </Button>
                    </View>
                  </CardItem>
                </Card>
              </CollapseBody>
            </Collapse>
            <Collapse
              isCollapsed={this.state.collapsedBtnAlihkan}
              onToggle={(isCollapsed) => this.setState({ collapsedBtnAlihkan: isCollapsed })}
            >
              <CollapseHeader />
              <CollapseBody style={{marginTop:-9}}>
                <Card transparent>
                  <CardItem style={{ flexDirection: 'column' }}>
                    <View style={{ width: '100%' }}>
                      <View>
                        <Text style={styles.text}>Jumlah Unit yang Akan Dialihkan</Text>
                        <Item regular style={styles.input}>
                          <Input />
                        </Item>
                        <View style={{ flexDirection: 'row', marginLeft: -7 }}>
                          <CheckBox
                            style={{ borderRadius: 3 }}
                            checked={this.state.checkedAlihkan}
                            onPress={() =>
                              this.setState({ checkedAlihkan: !this.state.checkedAlihkan })}
                          />
                          <Text style={{ fontSize: 12, marginLeft: 15, marginTop: 2 }}>
                            Semua Unit
                            </Text>
                        </View>
                      </View>
                      <View style={{ marginVertical: 10 }}>
                        <Text style={styles.text}>Estimasi Nilai Reksadana</Text>
                        <Text style={styles.text}>IDR. 0</Text>
                      </View>


                      <View style={{ marginTop: -20 }}>
                        <Dropdown
                          label="Dialihkan ke"
                          data={data}
                          onChangeText={this.updateItem}
                          style={{ fontSize: 12 }}
                        />
                      </View>

                      {alihkan ? (
                        <View>
                          <View style={{ marginVertical: 10 }}>
                            <Text style={styles.text}>NAB/Unit (2018-11-13 15:34:17)</Text>
                            <Text style={styles.text}>IDR. 1024.362887</Text>
                          </View>
                          <View>
                            <Text style={styles.text}>Biaya Pengalihan</Text>
                            <Text style={styles.text}>0.02%</Text>
                          </View>
                          <View style={{ marginVertical: 10 }}>
                            <Text style={styles.text}>Minimum Pengalihan</Text>
                            <Text style={styles.text}>IDR. 20.000</Text>
                          </View>
                          <Button
                            full
                            transparent
                            onPress={() => {
                              this.props.navigation.navigate('PortofolioAlihkan');
                            }}
                            style={{ height: 35, borderRadius: 3, backgroundColor:'#df514f' }}
                          >
                            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 14, color:'white' }}>
                              Alihkan
                              </Text>
                          </Button>
                        </View>
                      ) : null}
                    </View>
                  </CardItem>
                </Card>
              </CollapseBody>
            </Collapse>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 12
  },
  textCard: {
    fontSize: 12,
    fontWeight: 'bold'
  },
  input: {
    height: 35,
    borderRadius: 3,
    marginVertical: 5
  }
});

AppRegistry.registerComponent('propstate', () => Portofolio);
