import React, {Component} from 'react';
import {StyleSheet, AppRegistry, TouchableOpacity, ImageBackground, Image} from 'react-native';
import { Container,Content, Text, Form, Item, Input, Button, View, Drawer, Right, Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Footer from '../../../components/footer/Footer'
export default class Login extends Component {
  static navigationOptions = {
    header:null
  }
  constructor(){
    super();
    this.state = {
      password: null,
      email: null,
      token:null,
      token1:null
    }
  }

  async saveItem(item, selectedValue) {
    try {
      JSON.parse(await AsyncStorage.setItem(item, selectedValue)); 
      console.log(JSON.parse(await AsyncStorage.setItem(item, selectedValue)));
      
      alert(selectedValue)
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  userLogin () {
    if (!this.state.email || !this.state.password) return;
    axios.post('http://202.158.32.132:8085/api/auth/signin',{
          email:this.state.email,
          password:this.state.password
      }).then((response)=>{
        this.saveItem('token', response.data.token)
        this.setState({
          token1:response.data.token
        })
        this.props.navigation.navigate('Dashboard',{
          token: this.state.token1
        })
        // this.props.navigation.navigate('Home')
        alert(response.data.token)
        console.log(this.state.token1);
        console.log(response.data.token);
        
      }).catch((error)=>{
        console.log(error)
        alert(error)
      } 
    )}
  
    render() {
      return (
        <Container>
        <Content>
            <View style={{flexDirection:'row'}}>
              <Image source={require('../../../public/logo.png')} 
                  style={{width:120, height:120}}
              />
              <Right>
                  <Button transparent onPress={ () => this.props.navigation.toggleDrawer()}>
                  <Icon style={{color:'black'}} name='md-menu' />
                  </Button>
              </Right>
            </View>
            <View padder style={{justifyContent:'center', marginTop:20}}>
              <Text style={{textAlign:'center', fontSize: 16, fontWeight:'bold', marginBottom:12}}>
                Masuk ke Personal Area
              </Text>
              <Text style={{textAlign:'center', fontSize:12}}>
                Silahkan masukkan email dan sandi untuk masuk ke
                Personal Area. Anda dapat mengelola akun Anda dari Personal Area.
              </Text>
            </View>

            <Form style={{marginTop:15, marginRight:15}}>
              <Item>
                <Input onChangeText={(email) => this.setState({email})} value={this.state.email}
                style={{fontSize:12}} placeholder='EMAIL' textContentType={'emailAddress'} />
              </Item>
              <Item>
                <Input onChangeText={(password) => this.setState({password})} value={this.state.password}
                style={{fontSize:12}} placeholder='SANDI' secureTextEntry={true} />
              </Item>
            </Form>

            <View style={{marginTop:20}}>
              <Button bordered style={{alignSelf: 'center', height:35, borderColor:'#C1944F'}}
              onPress={this.userLogin.bind(this)}
              // onPress={()=>{this.props.navigation.navigate('Home')}}
              >
                <Text style={{color:'black', fontSize:12}}>
                  LOGIN
                </Text>
              </Button>
            </View>

            <View style={{justifyContent:'flex-end', alignItems:'flex-end', marginRight:15}} >
              <View>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ForgotPassword')}}>
                  <Text style={{fontSize:12}}> 
                    Lupa Sandi ?
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Regist')}}>
                  <Text style={{fontSize:12}}>
                    Daftar member baru
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{marginTop:15, marginHorizontal:15}}>
              <ImageBackground style={{width:'100%', height:150}} source={require('../../../public/assets/images/background_landing_page1.jpg')}>
                
                <View style={{marginLeft:15, marginVertical:10}}>
                  <Text style={{fontSize:14, color:'white'}}>
                    AYERS ASIA ASSET MANAGEMENT
                  </Text>
                  <Text style={{fontSize:12, color:'white'}}>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti.
                  </Text>

                  <View style={{marginTop:20}}>
                    <Button style={{height:40,backgroundColor:'#b5957b', alignSelf:'center' }} onPress={()=>{this.props.navigation.navigate('Regist')}}>
                      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Regist')}}>
                        <Text style={{fontSize:14, textAlign:'center'}}>
                          Daftar member baru
                        </Text>
                      </TouchableOpacity>
                    </Button>
                  </View>

                </View>
              </ImageBackground>
            </View>
          </Content>
          <Footer/>
        </Container>

      );
    }
  }
  AppRegistry.registerComponent('propstate',()=>Login);
