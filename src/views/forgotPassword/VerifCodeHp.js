import React, {Component} from 'react';
import {StyleSheet,AppRegistry, ImageBackground, TouchableOpacity, TextInput, Image} from 'react-native';
import { Container, Content, View, Text, Button, Icon, Right } from 'native-base';
import OtpInputs from 'react-native-otp-inputs';
import axios from 'axios';
import Footer from '../../../components/footer/Footer';

export default class VerifCodeHp extends Component{
    constructor(props){
        super(props);
        this.state={
            otp:null,
            phone:null,
            hash:null
        }
    }

    VerifCodeHp(){
        console.log(this.state.otp);
        console.log(this.props.navigation.state.params.phone);
        console.log(this.props.navigation.state.params.hash);
        
        if(!this.state.otp) return;
        axios.post('http://202.158.32.132:8085/api/auth/forgot-password/otp/key', {
          otp: this.state.otp,
          phone: this.props.navigation.state.params.phone,
          hash: this.props.navigation.state.params.hash  
        }).then((response)=>{
          console.log(response.data)
          this.props.navigation.navigate('NewPassword')
          alert('sukses')
        }).catch((error)=>{
          console.log(error);
          alert(error)
        })
      }

      ResendOTP(){
        if(!this.props.navigation.state.params.phone && this.props.navigation.state.params.hash) return;
        axios.post('http://202.158.32.132:8085/api/auth/forgot-password/otp', {
          phone: this.props.navigation.state.params.phone,
          otp:'',
          hash:this.props.navigation.state.params.hash
        }).then((response)=>{
          this.props.navigation.navigate('VerifCodeHp')
          console.log(response.data)
          alert('sukses')
        }).catch((error)=>{
          console.log(error);
          alert(error)
        })
      }

    render(){
        return(
            <Container> 
                <Content>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../../../public/logo.png')} 
                            style={{width:120, height:120}}
                        />
                        <Right>
                            <Button transparent onPress={ () => this.props.navigation.toggleDrawer()}>
                                <Icon style={{color:'black'}} name='md-menu' />
                            </Button>
                        </Right>
                    </View>

                    <View padder style={{justifyContent:'center', marginTop:20}}>
                        <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:15}}>
                            Silahkan Masukkan Kode Verifikasi
                        </Text>
                        <Text style={{textAlign:'center', fontSize:12}}>
                            Silahkan cek sms/pesan yang telah dikirimkan ke nomor Anda.
                        </Text>
                    </View>

                    <View padder style={{flexDirection:'row', justifyContent:'center'}}>
                      <OtpInputs inputStyles={{fontSize:12, backgroundColor:'white', borderColor:'black', color:'black'}}
                      focusedBorderColor={'#000ff'}
                      handleChange={(otp) => this.setState({otp})} numberOfInputs={5} />
                    </View>

                    <View style={{marginTop:20}}>
                        <Button bordered 
                        style={{alignSelf: 'center', height:35, borderColor:'#C1944F'}}
                        onPress={this.VerifCodeHp.bind(this)}
                        >
                            <Text style={{color:'black', fontSize: 12}}>
                                CONFIRM
                            </Text>
                        </Button>
                    </View>

                    <View style={{justifyContent:'flex-end', alignItems:'flex-end', marginRight:15, marginTop:15}} >
                        <Text style={{fontSize:12}}> 
                            Tidak menerima sms ?
                        </Text>
                        <TouchableOpacity onPress={this.ResendOTP.bind(this)}>
                            <Text style={{fontSize:12}}> 
                                Kirim Ulang
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{marginTop:30, marginHorizontal:15}}>
                        <ImageBackground style={{width:'100%', height:150}} source={require('../../../public/assets/images/background_landing_page1.jpg')}>
                            <View style={{marginLeft:15, marginVertical:10}}>
                                <Text style={{fontSize:14, color:'white'}}>
                                    AYERS ASIA ASSET MANAGEMENT
                                </Text>
                                <Text style={{fontSize:12, color:'white'}}>
                                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti.
                                </Text>

                                <View style={{marginTop:20}}>
                                    <Button style={{height:40,backgroundColor:'#b5957b', alignSelf:'center' }} onPress={()=>{this.props.navigation.navigate('Regist')}}>
                                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Regist')}}>
                                        <Text style={{fontSize:14, textAlign:'center'}}>
                                        Daftar member baru
                                        </Text>
                                    </TouchableOpacity>
                                    </Button>
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                </Content>
                <Footer/>
            </Container>
        );
    }
}

AppRegistry.registerComponent('propstate',()=>VerifCodeHp);
