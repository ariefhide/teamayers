import React, {Component} from 'react';
import {StyleSheet,AppRegistry, ImageBackground, TouchableOpacity, Image} from 'react-native';
import { Container, Content, View, Text, Button, Item, Input, Icon, Right } from 'native-base';
import Footer from '../../../components/footer/Footer';
import SideBar from '../../../components/header/SideBar';
import axios from 'axios';

export default class ForgotPassByHp extends Component {
    static navigationOptions = {
      header:null
    }
    constructor(){
      super();
      this.state={
        phone : null,
        hash:null
      }
    }

    ForgetPassHP(){
      if(!this.state.phone) return;
      axios.post('http://202.158.32.132:8085/api/auth/forgot-password/otp', {
        phone: this.state.phone
      }).then((response)=>{
        this.setState({
          hash:response.data.hash
        })
        this.props.navigation.navigate('VerifCodeHp',{
          phone:this.state.phone,
          hash:this.state.hash
        })
        console.log(this.state.hash)
        alert('sukses')
      }).catch((error)=>{
        console.log(error);
        alert(error)
      })
    }
    
    render() {
      return (
        <Container> 

          <Content>
            <View style={{flexDirection:'row'}}>
              <Image source={require('../../../public/logo.png')} 
                  style={{width:120, height:120}}
              />
              <Right>
                  <Button transparent onPress={ () => this.props.navigation.toggleDrawer()}>
                    <Icon style={{color:'black'}} name='md-menu' />
                  </Button>
              </Right>
            </View>

            <View padder style={{justifyContent:'center', marginTop:20}}>
              <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:15}}>
                Lupa Kata Sandi
              </Text>
              <Text style={{textAlign:'center', fontSize:12}}>
                Silahkan masukkan No Telepon untuk mendapatkan kode OTP.
              </Text>

              <Item>
                <Input maxLength={12}
                onChangeText={(phone)=>this.setState({phone})} value={this.state.phone}
                style={{fontSize:12}} placeholder='NO TELEPON' keyboardType="numeric" />
              </Item>
            </View>

            <View style={{marginTop:20}}>
              <Button bordered 
              style={{alignSelf: 'center', height:35, borderColor:'#C1944F'}}
              onPress={this.ForgetPassHP.bind(this)}
              >
                <Text style={{color:'black', fontSize:12}}>KIRIM</Text>
              </Button>
            </View>

            <View style={{justifyContent:'flex-end', alignItems:'flex-end', marginRight:15}} >
              <View>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Login')}}>
                  <Text style={{fontSize:12}}> 
                    Masuk ke Akun
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Regist')}}>
                  <Text style={{fontSize:12}}>
                    Daftar member baru
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            
            <View style={{marginTop:30, marginHorizontal:15}}>
              <ImageBackground style={{width:'100%', height:150}} source={require('../../../public/assets/images/background_landing_page1.jpg')}>
                
                <View style={{marginLeft:15, marginVertical:10}}>
                  <Text style={{fontSize:14, color:'white'}}>
                    AYERS ASIA ASSET MANAGEMENT
                  </Text>
                  <Text style={{fontSize:12, color:'white'}}>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti.
                  </Text>

                  <View style={{marginTop:20}}>
                    <Button style={{height:40,backgroundColor:'#b5957b', alignSelf:'center' }} onPress={()=>{this.props.navigation.navigate('Regist')}}>
                      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Regist')}}>
                        <Text style={{fontSize:14, textAlign:'center'}}>
                          Daftar member baru
                        </Text>
                      </TouchableOpacity>
                    </Button>
                  </View>
                </View>
              </ImageBackground>
            </View>
          </Content>
          <Footer/>
        </Container>
      );
    }
  }
  
  AppRegistry.registerComponent('propstate',()=>ForgotPassByHp);
