import React, { Component } from "react";
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import Modal from "react-native-modal";
import SignatureCapture from "react-native-signature-capture";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  CheckBox
} from "native-base";

export default class Step7 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signature: "",
      isSoulSold: false,
      ckbox: "",
      cek: false
    };
  }

  signaturePadChange(base64DataUrl) {
    this.setState({
      signature: base64DataUrl
    });
  }

  signaturePadError(error) {
    console.error(error);
  }

  nextPreprocess = () => {
    this.props.saveState(7, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  state = {
    isModalVisible: false,
    isModalNotif: false
  };

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  _toggleModalNotif = () =>
    this.setState({
      isModalVisible: false,
      isModalNotif: !this.state.isModalNotif
    });

  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Row>
                        <Form style={{ width: "100%" }}>
                          <View style={{ flex: 1, flexDirection: "row" }}>
                            <CheckBox
                              style={styles.ckbox}
                              checked={this.state.cek}
                              onPress={() =>
                                this.setState({ cek: !this.state.cek })
                              }
                            />
                            <Text style={styles.titlebox}>
                              Setuju dengan persyaratan dan ketentuan layanan
                              Ayers Asset Management
                            </Text>
                          </View>
                          <View style={{ flex: 1, flexDirection: "column" }}>
                            <Text style={styles.titlebox}>
                              Masukan tanda tangan digital
                            </Text>
                            <SignatureCapture
                              style={[{ flex: 1 }, styles.signature]}
                              ref="sign"
                              onSaveEvent={this._onSaveEvent}
                              onDragEvent={this._onDragEvent}
                              saveImageFileInExtStorage={false}
                              showNativeButtons={false}
                              showTitleLabel={false}
                              viewMode={"portrait"}
                            />

                            <View style={{ flex: 1, flexDirection: "row" }}>
                              <TouchableHighlight
                                style={styles.buttonStyle}
                                onPress={() => {
                                  this.resetSign();
                                }}
                              >
                                <Text style={{ color: "white" }}>Reset</Text>
                              </TouchableHighlight>
                            </View>
                          </View>
                        </Form>
                      </Row>
                      <Row style={{ marginTop: 30 }}>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this._toggleModal}
                          >
                            <Text style={{ color: "white" }}>Finish</Text>
                          </Button>
                          <Modal
                            isVisible={this.state.isModalVisible}
                            deviceHeight={1080}
                          >
                            <View style={{ flex: 1 }}>
                              <Card>
                                <CardItem>
                                  <Body>
                                    <Text>
                                      Silahkan Masukan Kode Otentikasi Anda
                                    </Text>
                                    <Item
                                      regular
                                      style={{
                                        marginBottom: 10,
                                        marginTop: 10
                                      }}
                                    >
                                      <Input style={{ height: 40 }} />
                                    </Item>
                                    <Button
                                      block
                                      style={{
                                        height: 35,
                                        backgroundColor: "#6C757D",
                                        marginRight: 250
                                      }}
                                      onPress={this._toggleModalNotif}
                                    >
                                      <Text style={{ color: "white" }}>
                                        Submit
                                      </Text>
                                    </Button>
                                  </Body>
                                </CardItem>
                              </Card>
                            </View>
                          </Modal>
                          <Modal
                            isVisible={this.state.isModalNotif}
                            deviceHeight={1080}
                          >
                            <View style={{ flex: 1 }}>
                              <Card>
                                <CardItem>
                                  <Body>
                                    <CardItem header bordered>
                                      <Body>
                                        <Text>Registrasi Berhasil</Text>
                                      </Body>
                                    </CardItem>
                                    <Text
                                      style={{
                                        marginBottom: 20,
                                        marginLeft: "5%"
                                      }}
                                    >
                                      Terima kasih telah menyelesaikan proses
                                      registrasi, data anda akan kami proses
                                    </Text>
                                    <Button
                                      block
                                      style={{
                                        height: 35,
                                        backgroundColor: "#6C757D",
                                        marginLeft: "5%",
                                        marginRight: "75%"
                                      }}
                                      onPress={this._toggleModalNotif}
                                    >
                                      <Text style={{ color: "white" }}>
                                        Home
                                      </Text>
                                    </Button>
                                  </Body>
                                </CardItem>
                              </Card>
                            </View>
                          </Modal>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }

  resetSign() {
    this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    console.log(result);
  }
  _onDragEvent() {
    // This callback will be called when the user enters signature
    console.log("dragged");
  }
}

const styles = StyleSheet.create({
  ckbox: {
    marginRight: 10,
    marginBottom: 30,
    marginTop: 5
  },
  titlebox: {
    marginLeft: 10,
    marginRight: 50
  },
  signature: {
    margin: 10,
    flex: 1,
    width: 250,
    height: 200,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "20%",
    backgroundColor: "#6C757D",
    marginBottom: 10,
    marginLeft: 10
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  }
});
