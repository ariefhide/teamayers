import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import RadioForm from "react-native-radio-form";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  Textarea,
  Left,
  Right
} from "native-base";
const que1 = [
  {
    label: "- 1 Tahun",
    value: "1"
  },
  {
    label: "1 - 3 Tahun",
    value: "2"
  },
  {
    label: "3 - 5 Tahun",
    value: "3"
  },
  {
    label: "> 5 Tahun",
    value: "4"
  }
];
const que2 = [
  {
    label:
      "Untuk keamanan dana investasi daripada pendapatan dan pertumbuhan dana",
    value: "1"
  },
  {
    label: "Untuk pendapatan dan keamanan dana investasi",
    value: "2"
  },
  {
    label: "Untuk pendapatan dan pertumbuhan jangka panjang",
    value: "3"
  },
  {
    label: "Untuk pendapatan dan pertumbuhan dana investasi yang agresif",
    value: "4"
  }
];
const que3 = [
  {
    label:
      "Tidak bersedia menerima risiko kehilangan nilai investasi awal. Saya/Kami akan segera menjual investasi tersebut apablia terjadi penurunan nilai atau kerugian",
    value: "1"
  },
  {
    label:
      "Bersedia menerima risiko penurunan nilai investasi sedikit dalam jangka pendek dengan harapan memperoleh imbalan hasil di atas suku bunga deposito.",
    value: "2"
  },
  {
    label:
      "Bersedia menerima risiko kehilangan sebagian dari nilai investasi awal",
    value: "3"
  },
  {
    label: "Bersedia menerima risiko kehilangan seluruh nilai investasi awal",
    value: "4"
  }
];
const que4 = [
  {
    label: "50%",
    value: "1"
  },
  {
    label: "50% - 75%",
    value: "2"
  },
  {
    label: "75% - 99%",
    value: "3"
  },
  {
    label: "100%",
    value: "4"
  }
];
const que5 = [
  {
    label:
      "Gaji/Tabungan/Deposito yang dalam jangka pendek akan digunakan untuk sehari-hari",
    value: "1"
  },
  {
    label: "Gaji/Tabungan/Deposito yang tidak digunakan dalam jangka pendek",
    value: "2"
  },
  {
    label: "Dana Menganggur                                                  .",
    value: "3"
  },
  {
    label:
      "Pengalihan investasi lain dengan tujuan meningkatkan pertumbuhan kekayaan saya secara singkat",
    value: "4"
  }
];
const que6 = [
  {
    label: "Tidak tahu sama sekali",
    value: "1"
  },
  {
    label: "Minim",
    value: "2"
  },
  {
    label: "Cukup Tahu",
    value: "3"
  },
  {
    label: "Sangat Tahu",
    value: "4"
  }
];
const que7 = [
  {
    label: "Tidak Tahu Sama Sekali",
    value: "1"
  },
  {
    label: "Minim",
    value: "2"
  },
  {
    label: "Cukup Tahu",
    value: "3"
  },
  {
    label: "Sangat Tahu",
    value: "4"
  }
];
const que8 = [
  {
    label:
      "Tidak Ada                                                                .",
    value: "1"
  },
  {
    label: "Reksa dana pendapatan tetap atau reksa dana pasar uang",
    value: "2"
  },
  {
    label: "Reksa dana campuran                                           .",
    value: "3"
  },
  {
    label: "Reksa dana saham                                                 .",
    value: "4"
  }
];

export default class Step6 extends Component {
  _onSelect = item => {};

  nextPreprocess = () => {
    this.props.saveState(6, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>1. </Text>
                          <Text>
                            Berapa lama anda berencana menginvestasikan dan anda
                            ?
                          </Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30 }}
                          dataSource={que1}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>2. </Text>
                          <Text>Tujuan investasi anda adalah ?</Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30, marginLeft: 15 }}
                          dataSource={que2}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>3. </Text>
                          <Text>Tingkat Resiko yang siap anda terima ?</Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30, marginLeft: 15 }}
                          dataSource={que3}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>4. </Text>
                          <Text>
                            Perkiraan persentase jumlah aset dari dana yang anda
                            investasikan (tidak termasuk properti/tempat tinggal
                            maupun aset bisnis anda)
                          </Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30 }}
                          dataSource={que4}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>5. </Text>
                          <Text>
                            Sumebr dari dana yang akan diinvestasikan ?
                          </Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30, marginLeft: 15 }}
                          dataSource={que5}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>6. </Text>
                          <Text>
                            Tingkat pengetahuan anda atas industri reksa dana
                            secara umum ?
                          </Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30 }}
                          dataSource={que6}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>7. </Text>
                          <Text>
                            Tingkat pengetahuan anda atas industri reksa dana
                            yang dimiliki ?
                          </Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30 }}
                          dataSource={que7}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.qu}>
                        <View style={{ flexDirection: "row" }}>
                          <Text rounded>8. </Text>
                          <Text>Reksa dana yang dimiliki saat ini ?</Text>
                        </View>
                        <RadioForm
                          style={{ width: 350 - 30, marginLeft: 15 }}
                          dataSource={que8}
                          itemShowKey="label"
                          itemRealKey="value"
                          circleSize={16}
                          initial={0}
                          formHorizontal={false}
                          labelHorizontal={true}
                          onPress={item => this._onSelect(item)}
                        />
                      </View>

                      <View style={styles.nilaiProfil}>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={styles.que}>
                            Nilai Total Profil Nasabah :{" "}
                          </Text>
                          <Text style={{ color: "#BEC2C5" }}>8</Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={styles.que}>PERNYATAAN </Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={styles.que}>
                            Saya/Kami menyatakan bahwa semua informasi yang
                            diberikan pada lembar ini adalah benar sesuai dengan
                            Profil investasi Saya/Kami. Dalam hal Saya/Kami
                            memilih produk investasi PT. Ayers Asia Asset
                            Management tidak sesuai dengan profil resiko
                            investasi sebagaimana disebutkan diatas, Saya/Kami
                            bersedia menerima resiko atas pilihan investasi
                            Saya/Kami
                          </Text>
                        </View>
                        <Form style={{ width: "100%", marginTop: 10 }}>
                          <Text>Tempat</Text>
                          <Item
                            style={{ marginBottom: 10, marginTop: 5 }}
                            regular
                          >
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text>Tanggal</Text>
                          <Item
                            style={{ marginBottom: 10, marginTop: 5 }}
                            regular
                          >
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text>Nama Lengkap</Text>
                          <Item
                            style={{ marginBottom: 10, marginTop: 5 }}
                            regular
                          >
                            <Input style={styles.boxsiz} />
                          </Item>
                        </Form>
                      </View>

                      <Row style={{ marginTop: 30 }}>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  quetions: {
    marginRight: "20%"
  },
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  que: {
    marginBottom: 5,
    color: "#BEC2C5"
  },
  qu: {
    marginBottom: 20,
    width: "90%"
  },
  nilaiProfil: {
    marginBottom: 20
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  }
});
