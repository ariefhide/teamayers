import React, { Component } from "react";
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  Image,
  TouchableOpacity,
  PixelRatio
} from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import RadioForm from "react-native-radio-form";
import ImagePicker from "react-native-image-picker";
import DatePicker from "react-native-datepicker";
import { Dropdown } from "react-native-material-dropdown";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon
} from "native-base";
const tandaPengenl = [
  {
    label: "KTP SEUMUR HIDUP",
    value: "KTP SEUMUR HIDUP"
  },
  {
    label: "KTP",
    value: "KTP"
  },
  {
    label: "PASSPORT",
    value: "PASSPORT"
  },
  {
    label: "KIMS",
    value: "KIMS"
  },
  {
    label: "KITAS",
    value: "KITAS"
  },
  {
    label: "KITAP",
    value: "KITAP"
  },
  {
    label: "SIM",
    label: "SIM"
  }
];
const pendidikan = [
  {
    label: "SD",
    value: "SD"
  },
  {
    label: "SMP",
    value: "SMP"
  },
  {
    label: "SMA",
    value: "SMA"
  },
  {
    label: "DIPLOMA",
    value: "DIPLOMA"
  },
  {
    label: "S1",
    value: "S1"
  },
  {
    label: "S2",
    value: "S2"
  },
  {
    label: "S3",
    label: "S3"
  },
  {
    label: "LAIN-LAIN",
    label: "LAIN-LAIN"
  }
];
export default class Step2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radbox: "",
      rad: false,
      date: "20/10/2019",
      text: ''
    };
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }

  state = {
    avatarSource: null
  };

  handleInputChange = (text) => {
    if (/^\d+$/.test(text)) {
      this.setState({
        text: text
      });
    }
  }


  _onSelect = item => { };

  nextPreprocess = () => {
    this.props.saveState(2, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // let source = { uri: response.uri };
        let source = { uri: "data:image/jpeg;base64," + response.data };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  render() {
    let dataPernikahan = [
      {
        value: "Menikah"
      },
      {
        value: "Belum Menikah"
      }
    ];
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Row>
                        <Form style={{ width: "100%" }}>
                          <Text style={styles.inputsiz}>Tanda Pengenal</Text>
                          <RadioForm
                            dataSource={tandaPengenl}
                            itemShowKey="label"
                            itemRealKey="value"
                            circleSize={16}
                            initial={0}
                            formHorizontal={true}
                            labelHorizontal={true}
                            onPress={item => this._onSelect(item)}
                          />
                          <Text style={styles.inputsiz}>
                            Tanggal Kadaluarsa
                          </Text>
                          <DatePicker
                            style={{ width: "100%", marginBottom: 10 }}
                            date={this.state.date}
                            mode="date"
                            format="DD/MM/YYYY"
                            minDate="25/05/2019"
                            maxDate="26/06/2026"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateIcon: {
                                width: 0,
                                height: 0,
                                marginRight: -5
                              },
                              dateInput: {
                                alignItems: "flex-start",
                                padding: 10,
                                backgroundColor: "#E9ECEF"
                              }
                              // ... You can check the source to find the other keys.
                            }}
                            onDateChange={date => {
                              this.setState({ date: date });
                            }}
                          />

                          <Text style={styles.inputsiz}>Nomor Identitas</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input
                              keyboardType={'numeric'}
                              style={styles.boxsiz}
                              onChangeText={this.handleInputChange}
                              value={this.state.text} />
                          </Item>
                          <Text style={styles.inputsiz}>
                            Unggah Foto Tanda Pengenal
                          </Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input
                              editable={false}
                              style={styles.boxsiz}
                              type="file"
                            />
                            <Button
                              style={styles.btnFile}
                              onPress={this.selectPhotoTapped.bind(this)}
                            >
                              <Text style={{ fontSize: 12 }}>Browse</Text>
                            </Button>
                          </Item>
                          <Text style={styles.inputsiz}>Nomor NPWP</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input
                              keyboardType={'numeric'}
                              style={styles.boxsiz}
                              onChangeText={this.handleInputChange}
                              value={this.state.text} />
                          </Item>
                          <Text style={styles.inputsiz}>Unggah Foto NPWP</Text>
                          <Item regular>
                            <Input
                              editable={false}
                              style={styles.boxsiz}
                              type="file"
                            />
                            <Button
                              style={styles.btnFile}
                              onPress={this.selectPhotoTapped.bind(this)}
                            >
                              <Text style={{ fontSize: 12 }}>Browse</Text>
                            </Button>
                          </Item>
                          <Dropdown
                            label="Status Pernikahan"
                            data={dataPernikahan}
                          />
                          <Text style={styles.inputsiz}>Nama Pasangan</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Nama Ahli Waris</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>
                            Hubungan Dengan Ahli Waris
                          </Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Pendidikan</Text>
                          <RadioForm
                            style={{ width: 350 }}
                            dataSource={pendidikan}
                            itemShowKey="label"
                            itemRealKey="value"
                            circleSize={16}
                            initial={0}
                            formHorizontal={true}
                            labelHorizontal={true}
                            onPress={item => this._onSelect(item)}
                          />
                        </Form>
                      </Row>
                      <Row style={{ marginTop: 30 }}>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  },
  radiob: {
    marginLeft: 5,
    marginRight: 5
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  }
});
