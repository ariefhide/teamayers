import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Button, Right, Icon, Container, Content } from "native-base";
import MultiStep from "react-native-multistep-wizard";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Step4 from "./Step4";
import Step5 from "./Step5";
import Step6 from "./Step6";
import Step7 from "./Step7";
import Footer from "../../../components/footer/Footer";

export default class Regist extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      steps: [
        { name: "Step1", component: <Step1 /> },
        { name: "Step2", component: <Step2 /> },
        { name: "Step3", component: <Step3 /> },
        { name: "Step4", component: <Step4 /> },
        { name: "Step5", component: <Step5 /> },
        { name: "Step6", component: <Step6 /> },
        { name: "Step7", component: <Step7 /> }
      ]
    };
  }

  finish(wizardState) {}

  render() {
    const { steps } = this.state;
    return (
      <Container>
        <Content>
        <View style={{flexDirection:'row'}}>
          <Image source={require('../../../public/logo.png')} 
              style={{width:120, height:120}}
          />
          <Right>
              <Button transparent onPress={ () => this.props.navigation.toggleDrawer()}>
              <Icon style={{color:'black'}} name='md-menu' />
              </Button>
          </Right>
        </View>
        <MultiStep steps={steps} onFinish={this.finish} />
        </Content>
        <Footer/>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});

AppRegistry.registerComponent("propstate", () => Regist);
