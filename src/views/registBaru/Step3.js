import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import DatePicker from "react-native-datepicker";
import { Dropdown } from "react-native-material-dropdown";
import RadioForm from "react-native-radio-form";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  Textarea
} from "native-base";
const jenisKelamin = [
  {
    label: "PRIA",
    value: "PRIA"
  },
  {
    label: "PEREMPUAN",
    value: "PEREMPUAN"
  }
];

export default class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: "20/10/1980"
    };
  }

  _onSelect = item => {};

  nextPreprocess = () => {
    this.props.saveState(3, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  render() {
    let dataAgama = [
      {
        value: "Islam"
      },
      {
        value: "Kristen"
      }
    ];
    let dataKewarganegaraan = [
      {
        value: "INDONESIA"
      },
      {
        value: "LAINNYA"
      }
    ];
    let dataProvinsi = [
      {
        value: "JAWA BARAT"
      },
      {
        value: "LAINNYA"
      }
    ];
    let dataKota = [
      {
        value: "JAKARTA"
      },
      {
        value: "LAINNYA"
      }
    ];
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Row>
                        <Form style={{ width: "100%" }}>
                          <Text style={styles.inputsiz}>Tempat Lahir</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Tanggal Lahir</Text>
                          <DatePicker
                            style={{ width: "100%", marginBottom: 10 }}
                            date={this.state.date}
                            mode="date"
                            format="DD/MM/YYYY"
                            minDate="25/05/2019"
                            maxDate="26/06/2026"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateIcon: {
                                width: 0,
                                height: 0,
                                marginRight: -5
                              },
                              dateInput: {
                                alignItems: "flex-start",
                                padding: 10
                              }
                              // ... You can check the source to find the other keys.
                            }}
                            onDateChange={date => {
                              this.setState({ date: date });
                            }}
                          />
                          <Dropdown label="Agama" data={dataAgama} />
                          <Text style={styles.inputsiz}>Jenis Kelamin</Text>
                          <RadioForm
                            dataSource={jenisKelamin}
                            itemShowKey="label"
                            itemRealKey="value"
                            circleSize={16}
                            initial={1}
                            formHorizontal={true}
                            labelHorizontal={true}
                            onPress={item => this._onSelect(item)}
                          />
                          <Text style={styles.inputsiz}>Nomor Telpon</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input 
                             keyboardType={'numeric'} 
                             style={styles.boxsiz}  
                             onChangeText={this.handleInputChange}    
                             value={this.state.text}/>
                            </Item>
                          <Text style={styles.inputsiz}>Nomor Faksmili</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input  keyboardType={'numeric'} 
                             style={styles.boxsiz}  
                             onChangeText={this.handleInputChange}    
                             value={this.state.text}/>
                          </Item>
                          <Text style={styles.inputsiz}>
                            Alamat Domisili Sesuai KTP
                          </Text>
                          <Form>
                            <Textarea rowSpan={5} bordered />
                          </Form>
                          <Dropdown
                            label="Kewarganegaraan"
                            data={dataKewarganegaraan}
                          />
                          <Dropdown label="Provinsi" data={dataProvinsi} />
                          <Dropdown label="Kota" data={dataKota} />
                          <Text style={styles.inputsiz}>Kecamatan</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                        </Form>
                      </Row>
                      <Row style={{ marginTop: 30 }}>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  radiob: {
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 10
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  },
  silahkan: {
    fontSize: 15,
    textAlign: "center",
    margin: 10
  }
});
