import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Dropdown } from "react-native-material-dropdown";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  Textarea
} from "native-base";

export default class Step4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  handleInputChange = (text) => {
    if (/^\d+$/.test(text)) {
      this.setState({
        text: text
      });
    }
  }

  _onSelect = item => {};

  nextPreprocess = () => {
    this.props.saveState(4, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  render() {
    let dataPekerjaan = [
      {
        value: "GURU"
      },
      {
        value: "LAINNYA"
      }
    ];
    let dataSumberDana = [
      {
        value: "KEUNTUNGAN BISNIS"
      },
      {
        value: "LIANNYA"
      }
    ];
    let dataPenghasilanPertahun = [
      {
        value: "< 10 MILLION/YEAR"
      },
      {
        value: "...."
      }
    ];
    let dataTujuanInvestasi = [
      {
        value: "KEUNTUNGAN DARI PERBEDAAN HARGA"
      },
      {
        value: "...."
      }
    ];
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Row>
                        <Form style={{ width: "100%" }}>
                          <Dropdown label="Pekerjaan" data={dataPekerjaan} />
                          <Text style={styles.inputsiz}>Nama Perusahaan</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Jabatan</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Alamat Perusahaan</Text>
                          <Item regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Dropdown label="Sumber Dana" data={dataSumberDana} />
                          <Text style={styles.inputsiz}>
                            Nomor Telpon Perusahaan
                          </Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input  keyboardType={'numeric'} 
                             style={styles.boxsiz}  
                             onChangeText={this.handleInputChange}    
                             value={this.state.text}/>
                          </Item>
                          <Dropdown
                            label="Penghasilan Pertahun"
                            data={dataPenghasilanPertahun}
                          />
                          <Dropdown
                            label="Tujuan Investasi"
                            data={dataTujuanInvestasi}
                          />
                        </Form>
                      </Row>
                      <Row style={{ marginTop: 30 }}>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  radiob: {
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 10
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  },
  silahkan: {
    fontSize: 15,
    textAlign: "center",
    margin: 10
  }
});
