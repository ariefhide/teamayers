import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image, TextInput } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon
} from "native-base";

export default class Step1 extends Component {
  nextPreprocess = () => {
    this.props.saveState(2, { key: "value" });
    this.props.nextFn();
  };

  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 270 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Row>
                        <Form style={{ width: "100%" }}>
                          <Text style={styles.inputsiz}>Nama</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Alamat Email</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input keyboardType={'email-address'} style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>Kata Sandi</Text>
                          <Item style={{ marginBottom: 10 }} regular>
                            <Input secureTextEntry={true} style={styles.boxsiz} />
                          </Item>
                          <Text style={styles.inputsiz}>
                            Konfirmasi Kata Sandi
                          </Text>
                          <Item style={{ marginBottom: 30 }} regular>
                            <Input secureTextEntry={true} style={styles.boxsiz} />
                          </Item>
                        </Form>
                      </Row>
                      <Row>
                        <Col size={7} />
                        <Col size={1.5} />
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  silahkan: {
    fontSize: 15,
    textAlign: "center",
    margin: 10
  }
});
