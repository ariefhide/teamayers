import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import Tab1 from "./tabx/Tab1";
import Tab2 from "./tabx/Tab2";
import Tab3 from "./tabx/Tab3";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  Textarea,
  Tabs,
  Tab
} from "native-base";

export default class Step5 extends Component {
  nextPreprocess = () => {
    this.props.saveState(5, { key: "value" });
    this.props.nextFn();
  };

  previousPreprocess = () => {
    this.props.prevFn();
  };

  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Col style={{ width: 220 }}>
              <Text style={styles.judul}>Register Persoanl Area</Text>
            </Col>
            <Col />
          </Grid>
          <Grid>
            <Col style={{ margin: 5 }}>
              <Card>
                <CardItem>
                  <Body>
                    <Grid>
                      <Col>
                        <Tabs
                          tabBarUnderlineStyle={{ backgroundColor: "#A1887F" }}
                        >
                          <Tab
                            heading="Bank 1"
                            tabStyle={{ backgroundColor: "white" }}
                            textStyle={{ color: "#E3E3E5" }}
                            activeTabStyle={{ backgroundColor: "#ffffff" }}
                            activeTextStyle={{
                              color: "#151515",
                              fontWeight: "normal"
                            }}
                          >
                            <Tab1 />
                          </Tab>
                          <Tab
                            heading="Bank 2"
                            tabStyle={{ backgroundColor: "white" }}
                            textStyle={{ color: "#E3E3E5" }}
                            activeTabStyle={{ backgroundColor: "#ffffff" }}
                            activeTextStyle={{
                              color: "#151515",
                              fontWeight: "normal"
                            }}
                          >
                            <Tab2 />
                          </Tab>
                          <Tab
                            heading="Bank 3"
                            tabStyle={{ backgroundColor: "white" }}
                            textStyle={{ color: "#E3E3E5" }}
                            activeTabStyle={{ backgroundColor: "#ffffff" }}
                            activeTextStyle={{
                              color: "#151515",
                              fontWeight: "normal"
                            }}
                          >
                            <Tab3 />
                          </Tab>
                        </Tabs>
                      </Col>
                      <Row>
                        <Col size={5} />
                        <Col size={2}>
                          <Button
                            block
                            style={{
                              height: 35,
                              backgroundColor: "#A1887F",
                              marginRight: 10
                            }}
                            onPress={this.previousPreprocess}
                          >
                            <Text style={{ color: "white" }}>Previous</Text>
                          </Button>
                        </Col>
                        <Col size={1.5}>
                          <Button
                            block
                            style={{ height: 35, backgroundColor: "#A1887F" }}
                            onPress={this.nextPreprocess}
                          >
                            <Text style={{ color: "white" }}>Next</Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row style={styles.container}>
                        <View style={{ flexDirection: "row" }}>
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#A1887F"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                          <Icon
                            style={{
                              marginRight: 2,
                              fontSize: 18,
                              color: "#BBBBBB"
                            }}
                            type="FontAwesome"
                            name="circle"
                          />
                        </View>
                      </Row>
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  radiob: {
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 10
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  },
  silahkan: {
    fontSize: 15,
    textAlign: "center",
    margin: 10
  }
});
