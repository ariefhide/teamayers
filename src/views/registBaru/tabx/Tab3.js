import React, { Component } from "react";
import { StyleSheet, AppRegistry, Text, View, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Dropdown } from "react-native-material-dropdown";
import {
  Container,
  Header,
  Button,
  ListItem,
  Radio,
  Content,
  Card,
  CardItem,
  Body,
  Item,
  Input,
  Form,
  Icon,
  Textarea
} from "native-base";

export default class Tab3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  handleInputChange = (text) => {
    if (/^\d+$/.test(text)) {
      this.setState({
        text: text
      });
    }
  }

  _onSelect = item => {};

  render() {
    let dataSwift = [
      {
        value: "Tabungan"
      },
      {
        value: "Lainnya"
      }
    ];
    let dataMataUang = [
      {
        value: "IDR"
      },
      {
        value: "US Dollar"
      }
    ];
    let dataBank = [
      {
        value: "Mandiri"
      },
      {
        value: "....."
      }
    ];
    return (
      <Card style={{ marginBottom: 20 }}>
        <CardItem>
          <Body>
            <Form style={{ width: "100%" }}>
              <Dropdown label="Nama Bank" data={dataBank} />
              <Text style={styles.inputsiz}>Nomor Rekening</Text>
              <Item style={{ marginBottom: 10 }} regular>
                <Input keyboardType={'numeric'} 
                             style={styles.boxsiz}  
                             onChangeText={this.handleInputChange}    
                             value={this.state.text}/>
              </Item>
              <Text style={styles.inputsiz}>Cabang</Text>
              <Item style={{ marginBottom: 10 }} regular>
                <Input style={styles.boxsiz} />
              </Item>
              <Text style={styles.inputsiz}>Rekening Atas Nama</Text>
              <Item regular>
                <Input style={styles.boxsiz} />
              </Item>
              <Dropdown label="Kode SWIFT" data={dataSwift} />
              <Dropdown label="Mata Uang" data={dataMataUang} />
            </Form>
          </Body>
        </CardItem>
      </Card>
    );
  }
}
const styles = StyleSheet.create({
  inputsiz: {
    marginBottom: 10,
    fontSize: 16,
    height: 20
  },
  radiob: {
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 10
  },
  judul: {
    marginLeft: 40,
    marginTop: 20,
    fontSize: 22,
    borderTopWidth: 5,
    borderTopColor: "#A1887F"
  },
  boxsiz: {
    height: 38,
    fontSize: 16
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  btnFile: {
    width: 60,
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#BBBBBB"
  },
  silahkan: {
    fontSize: 15,
    textAlign: "center",
    margin: 10
  }
});
