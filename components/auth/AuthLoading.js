import React, {Component} from 'react';
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
export default class AuthLoadingScreen extends Component {
  constructor() {
    super();
    this.state = { 
      hasToken: false,
      isLoaded: false
     };
  }

  componentDidMount() {
    AsyncStorage.getItem('token').then((token) => {
      this.setState({ 
        hasToken: token !== null,
       })
      //  alert(token)
       this.props.navigation.navigate(token ? 'Dashboard' : 'App');
    })
  }

  render() {
    return (
      <View>
      </View>
    )};
  
}