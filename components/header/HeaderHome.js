import React, {Component} from 'react';
import {StyleSheet,AppRegistry,Image, Dimensions, AsyncStorage} from 'react-native';
import { Container, Content, Drawer, Right, Button, Icon, Text, View, } from 'native-base';
// import HeaderHome from '../../../components/header/HeaderHome'
// import SideBarHome from '../../../components/header/SideBarHome';

export default class Home extends Component {
  // static navigationOptions = {
  //   header:null
  // }
  _showMoreApp = () => {
    this.props.navigation.navigate('Other');
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

    render() {
      return (
        <Container> 
            <Content>
              <View style={{flexDirection:'row', backgroundColor:'#a1887f'}}>
                <Image source={require('../../public/assets/images/ayerswhite.png')} 
                    style={{margin:10, width:'30%', height:100}} resizeMode={'contain'}
                />
                <Right>
                    <Button transparent>
                      <Icon style={{color:'white'}} name='md-menu' />
                    </Button>
                </Right>
              </View>
              <View>
                <Button onPress={ () => this.props.navigation.toggleDrawer()} >
                  <Text style={{ fontSize:12 }}>logout</Text>
                </Button>
              </View>
            </Content>
        </Container>
      );
    }
  }
  
  AppRegistry.registerComponent('propstate',()=>Home);
  