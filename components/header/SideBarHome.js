import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import { Container,Content, View, Text, Right, Icon, Button } from 'native-base';
import { TouchableOpacity} from 'react-native';
import { DrawerActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

class DrawerScreen extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  userSignOut = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('App');
  };

  render () {
    return (
      <View style={{marginLeft:15, marginTop: 45}}>
        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
          <Icon style={{ color: 'black', marginLeft:'80%', marginBottom: 0 }} name='md-arrow-forward' />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToScreen('Home')}>
          <Text style={{marginTop:15, fontSize:14}}>
            Home
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToScreen('Transaction')}>
          <Text style={{marginTop:15, fontSize:14}}>
            Transaction
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToScreen('Portofolio')}> 
          <Text style={{marginTop:15, fontSize:14}}>
            Portfolio
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToScreen('Profile')}>
          <Text style={{marginTop:15, fontSize:14}}>
            Profile
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.userSignOut}>
          <Text style={{marginTop:15, fontSize:14}}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

export default DrawerScreen;