import React, {Component} from 'react';
import { Container,Content, View, List, ListItem, Text, Icon } from 'native-base';
import {Image, TouchableOpacity} from 'react-native';
import { DrawerActions, NavigationActions } from 'react-navigation';
export default class SideBar extends Component{
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }
    render(){
        return(
        <Container>
          <Content style={{backgroundColor:'#FFFFFF'}}>
            <View style={{marginLeft:10}}>
              <View style={{flexDirection:'row', marginTop:15}}>
                <Image style={{width:20, height:20, marginRight:5}} source={require('../../public/assets/images/cs-images.png')}/>
                <Text style={{fontSize:12, color:'#917764'}}>
                  +62 21 2788 9428
                </Text>
              </View>
              <TouchableOpacity>
                <Image source={require('../../public/assets/icon/ID.png')} style={{marginTop:15}}/>
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={require('../../public/assets/icon/EN.png')} style={{marginTop:15}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.navigateToScreen('Login')}>
                <Text style={{marginTop:12, color:'#6c757d', fontSize:12}}>
                  Home
                </Text>
              </TouchableOpacity>
            </View>
          </Content>
        </Container>
        );
    }
}
