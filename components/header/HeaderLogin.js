import React, {Component} from 'react';
import { View, Drawer, Header, Left, Right, Button, Icon} from 'native-base';
import {Image} from 'react-native';
import SideBar from './SideBar'
export default class HeaderLogin extends Component{
    closeDrawer () {
        this.drawer._root.close()
      };

      openDrawer () {
        this.drawer._root.open()
      };
    render(){
        return(
            <View style={{height:120}}>
            <Drawer ref={(ref) => { this.drawer = ref; }} 
            side='right'
            content={<SideBar navigator={this.navigator}/>} 
            onClose={() => this.closeDrawer()}> 
            <View style={{flexDirection:'row'}}>
                <Image source={require('../../public/logo.png')} 
                    style={{width:120, height:120}}
                />
                <Right>
                    <Button transparent onPress={()=>{this.openDrawer()}}>
                    <Icon style={{color:'black'}} name='md-menu' />
                    </Button>
                </Right>
            </View>
            </Drawer>
            </View>
        );
    }
}
