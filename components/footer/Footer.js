import React, {Component} from 'react';
import {View} from 'native-base';
import {Image} from 'react-native';
export default class Footer extends Component{
    render(){
        return(
          <View style={{marginVertical:5}}>
            <Image style={{width:'100%', height:30}} source={require('../../public/assets/images/footer.png')} />
          </View>
        );
    }
}
